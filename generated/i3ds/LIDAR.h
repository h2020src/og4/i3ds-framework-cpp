#ifndef GENERATED_ASN1SCC_LIDAR_H
#define GENERATED_ASN1SCC_LIDAR_H
/*
Code automatically generated by asn1scc tool
*/
#include "Point.h"
#include "taste-types.h"
#include "Region.h"
#include "SampleAttribute.h"
#include "asn1crt.h"

#ifdef  __cplusplus
extern "C" {
#endif



typedef struct
{
  int nCount;

  Point arr[750000];
} LIDARMeasurement750K_points;

#define LIDARMeasurement750K_points_REQUIRED_BYTES_FOR_ENCODING       29437514
#define LIDARMeasurement750K_points_REQUIRED_BITS_FOR_ENCODING        235500112
#define LIDARMeasurement750K_points_REQUIRED_BYTES_FOR_ACN_ENCODING   29437503
#define LIDARMeasurement750K_points_REQUIRED_BITS_FOR_ACN_ENCODING    235500020
#define LIDARMeasurement750K_points_REQUIRED_BYTES_FOR_XER_ENCODING   171000059

void LIDARMeasurement750K_points_Initialize(LIDARMeasurement750K_points* pVal);
flag LIDARMeasurement750K_points_IsConstraintValid(const LIDARMeasurement750K_points* val, int* pErrCode);
#ifndef ERR_LIDARMeasurement750K_points
#define ERR_LIDARMeasurement750K_points		1001  /*(SIZE(1 .. 750000))*/
#endif

typedef struct
{
  SampleAttributes attributes;
  LIDARMeasurement750K_points points;
  PolarRegion region;
} LIDARMeasurement750K;

#define LIDARMeasurement750K_REQUIRED_BYTES_FOR_ENCODING       29437633
#define LIDARMeasurement750K_REQUIRED_BITS_FOR_ENCODING        235501057
#define LIDARMeasurement750K_REQUIRED_BYTES_FOR_ACN_ENCODING   29437621
#define LIDARMeasurement750K_REQUIRED_BITS_FOR_ACN_ENCODING    235500965
#define LIDARMeasurement750K_REQUIRED_BYTES_FOR_XER_ENCODING   171001304

void LIDARMeasurement750K_Initialize(LIDARMeasurement750K* pVal);
flag LIDARMeasurement750K_IsConstraintValid(const LIDARMeasurement750K* val, int* pErrCode);


typedef struct
{
  T_Boolean enable;
  PolarRegion region;
} LIDARRegion;

#define LIDARRegion_REQUIRED_BYTES_FOR_ENCODING       53
#define LIDARRegion_REQUIRED_BITS_FOR_ENCODING        417
#define LIDARRegion_REQUIRED_BYTES_FOR_ACN_ENCODING   53
#define LIDARRegion_REQUIRED_BITS_FOR_ACN_ENCODING    417
#define LIDARRegion_REQUIRED_BYTES_FOR_XER_ENCODING   350

void LIDARRegion_Initialize(LIDARRegion* pVal);
flag LIDARRegion_IsConstraintValid(const LIDARRegion* val, int* pErrCode);


typedef struct
{
  T_Boolean region_enabled;
  PolarRegion region;
} LIDARConfiguration;

#define LIDARConfiguration_REQUIRED_BYTES_FOR_ENCODING       53
#define LIDARConfiguration_REQUIRED_BITS_FOR_ENCODING        417
#define LIDARConfiguration_REQUIRED_BYTES_FOR_ACN_ENCODING   53
#define LIDARConfiguration_REQUIRED_BITS_FOR_ACN_ENCODING    417
#define LIDARConfiguration_REQUIRED_BYTES_FOR_XER_ENCODING   380

void LIDARConfiguration_Initialize(LIDARConfiguration* pVal);
flag LIDARConfiguration_IsConstraintValid(const LIDARConfiguration* val, int* pErrCode);




/* ================= Encoding/Decoding function prototypes =================
 * These functions are placed at the end of the file to make sure all types
 * have been declared first, in case of parameterized ACN encodings
 * ========================================================================= */

flag LIDARMeasurement750K_points_Encode(const LIDARMeasurement750K_points* val, BitStream* pBitStrm, int* pErrCode,
                                        flag bCheckConstraints);
flag LIDARMeasurement750K_points_Decode(LIDARMeasurement750K_points* pVal, BitStream* pBitStrm, int* pErrCode);
flag LIDARMeasurement750K_Encode(const LIDARMeasurement750K* val, BitStream* pBitStrm, int* pErrCode,
                                 flag bCheckConstraints);
flag LIDARMeasurement750K_Decode(LIDARMeasurement750K* pVal, BitStream* pBitStrm, int* pErrCode);
flag LIDARRegion_Encode(const LIDARRegion* val, BitStream* pBitStrm, int* pErrCode, flag bCheckConstraints);
flag LIDARRegion_Decode(LIDARRegion* pVal, BitStream* pBitStrm, int* pErrCode);
flag LIDARConfiguration_Encode(const LIDARConfiguration* val, BitStream* pBitStrm, int* pErrCode,
                               flag bCheckConstraints);
flag LIDARConfiguration_Decode(LIDARConfiguration* pVal, BitStream* pBitStrm, int* pErrCode);


#ifdef  __cplusplus
}

#endif

#endif
