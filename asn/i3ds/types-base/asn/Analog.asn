--------------------------------------------------------------------------------
--- ASN.1 message definitions for the I3DS sensor suite.
---
--- Copyright (C) 2017      Stiftelsen SINTEF
--- Copyright (C) 2018      SINTEF AS
---
--- This program is free software; you can redistribute it and/or
--- modify it under the terms of the GNU General Public License
--- as published by the Free Software Foundation; either version 2
--- of the License, or (at your option) any later version.
---
--- This program is distributed in the hope that it will be useful,
--- but WITHOUT ANY WARRANTY; without even the implied warranty of
--- MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
--- GNU General Public License for more details.
---
--- You should have received a copy of the GNU General Public License
--- along with this program;  if not, see <http://www.gnu.org/licenses/>.
--------------------------------------------------------------------------------

Analog-Types DEFINITIONS ::=
BEGIN
IMPORTS BatchSize FROM Sensor-Types
SampleAttributes FROM SampleAttribute-Types
T-UInt8, T-UInt32 FROM TASTE-BasicTypes
T-Float FROM TASTE-ExtendedTypes;

--- Series count is a static configuration of the analog sensor.
SeriesCount ::= T-UInt8

--- Analog series.
AnalogSamples{T-UInt32: maxSamples} ::= SEQUENCE (SIZE(0..maxSamples)) OF T-Float

--- Analog measurement, where number of samples is series count * batch size
AnalogMeasurement{T-UInt32: maxSamples} ::=
SEQUENCE
{
   attributes   SampleAttributes,
   samples      AnalogSamples{maxSamples},
   series       SeriesCount,
   batch-size   BatchSize
}

--- Analog measurement for 1000 sample points.
AnalogMeasurement1K ::= AnalogMeasurement{1000}

END
